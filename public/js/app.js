(function(undefined) {
    const container = document.querySelector('.gameContainer');
    /**
     * Time to keep the number pressed to initialise a lock
     * @type {number}
     */
    const LOCK_TIMEOUT = 600; // 0.6 seconds
    const gameRows = 5;
    const gameColumns = 5;

    /**
     * All initialised game cell
     *
     * @type { GameCell[] }
     */
    let gameCells = [];

    /**
     * Totals per row
     *
     * @type { number[] }
     */
    let rowTotals = [];

    /**
     * Totals per column
     *
     * @type { number[] }
     */
    let columnTotals = [];

    /**
     * Game cell class
     *
     * @param value
     * @param nodeEl
     * @return {{ GameCell }}
     */
    function GameCell(value, nodeEl)
    {
        const _value = value;
        const cellEl = nodeEl;

        let self = this;
        let pressTimer;

        /**
         * Initial state of the cell
         * @type {boolean}
         */
        let _isActive = true;
        let _isLocked = false;

        function cellPressed()
        {
            pressTimer = new Date();
        }

        function cellRelease()
        {
            let releaseTimer = new Date();
            if ((releaseTimer - pressTimer) > LOCK_TIMEOUT) {
                return lockCell();
            }
            cellClick();
        }

        function lockCell()
        {
            cellEl.classList.toggle('locked');
            _isLocked = !_isLocked;
        }

        // Basic lock on an element
        function cellClick()
        {
            cellEl.classList.toggle('inactive');
            _isActive = !_isActive;
            calculateTotals();
            applyTotals()
        }

        /**
         * Bind Events
         */
        // Lock/Tap events start
        cellEl.addEventListener('mousedown', cellPressed);
        cellEl.addEventListener('touchstart', cellPressed);
        // Lock/Tap events end
        cellEl.addEventListener('mouseup', cellRelease);
        cellEl.addEventListener('touchend', cellPressed);


        /**
         * Get state functions
         */
        self.getValue = () => { return _value; };
        self.isLocked = () => { return _isLocked; };
        self.isActive = () => { return _isActive; };

        return self;
    }

    function calculateTotals()
    {
        let currRow, currColl;
        rowTotals = [];
        columnTotals = [];

        for (currRow = 1; currRow <= gameRows; currRow++) {
            if (rowTotals[currRow] === undefined) {
                rowTotals[currRow] = 0;
            }
            for (currColl = 1; currColl <= gameColumns; currColl++) {
                /** @type {GameCell} currCell*/
                let currCell = gameCells[currRow][currColl];

                if (columnTotals[currColl] === undefined) {
                    columnTotals[currColl] = 0
                }

                if (currCell.isActive()) {
                    rowTotals[currRow] += currCell.getValue();
                    columnTotals[currColl] += currCell.getValue();
                }
            }
        }
    }

    function createCells()
    {
        let cellElements = document.querySelectorAll('cell.gameCell');
        cellElements.forEach(function(currentCell) {
            let cellColumn = parseInt(currentCell.dataset.col);
            let cellRow = parseInt(currentCell.dataset.row);

            let cellValue = parseInt(currentCell.textContent);

            if (gameCells[cellRow] === undefined) {
                gameCells[cellRow] = [];
            }
            gameCells[cellRow][cellColumn] = new GameCell(cellValue, currentCell);
        });
    }

    /**
     * Start a new game, calculate the row and column totals, initialise cells
     */
    function startNewGame() {
        createCells();
        calculateTotals();
        applyTotals();
    }

    function applyTotals()
    {
        for (let i = 1; i <= gameColumns; i++) {
            let totalCells = document.querySelectorAll('cell.totalCell[data-col="' + i +'"]');

            totalCells.forEach(function(eachCell) {
                eachCell.textContent = columnTotals[i];
            })
        }

        for (let i = 1; i <= gameRows; i++) {
            let totalCells = document.querySelectorAll('cell.totalCell[data-row="' + i + '"]');
            totalCells.forEach(function(eachCell) {
                eachCell.textContent = rowTotals[i];


            })
        }
    }

    function init() {
        if (document.readyState === 'complete') {
            startNewGame();
            return;
        }
        setTimeout(init, 100);
    }

    init();
})();